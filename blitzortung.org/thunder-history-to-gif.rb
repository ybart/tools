#!/usr/bin/env ruby

require 'open-uri'

def download(url, path)
  File.open(path, "w") do |f|
    IO.copy_stream(open(url), f)
  end
end

class ThunderGif
  SECONDS = 1
  MINUTES = 60 * SECONDS
  HOURS   = 60 * MINUTES
  DAYS    = 24 * HOURS
  BASE_URL = 'http://fr.blitzortung.org/archive_data.php'

  MAPS = {
    europe_so: 13
  }.freeze

  def initialize(end_date: self.default_end, frames: 48, interval: 30 * MINUTES,
                 map: :europe_so, scale: 120 * MINUTES, format: :gif)
    @end_date = end_date
    @frames = frames
    @interval = interval
    @map = map
    @scale = scale
    @format = format
  end

  def start_date
    @end_date - (@frames * @interval)
  end

  def default_end
    now = Time.now.utc
    now - (now.min * MINUTES + now.sec * SECONDS)
  end

  def query_parameters
    data_attributes.merge!(animation_attributes)
                   .merge!(date_attributes)
                   .merge!(unsupported_attributes)
  end

  def data_attributes
    {
      map: MAPS[@map],
      agespan: (@scale / (60 * SECONDS)).to_i,
    }
  end

  def animation_attributes
    {
      png_gif: format_for_query,
      frames: @frames,
      delay: 150,
      last_delay: 2000,
    }
  end

  def date_attributes
    {
      start_date: date_for_query(start_date),
      start_time: time_for_query(start_date) - start_delta(start_date),
      end_date: date_for_query(@end_date),
      end_time: time_for_query(@end_date),
    }
  end

  def unsupported_attributes
    # Not supported with this tool (but API might support these)
    {
      stations_users: 0,
      selected_numbers: '*',
      north: 90, west: -180, east: 180, south: -90,
      rawdata_image: 1,
      width_orig: 640,
      width_result: 640,
      show_result: 1
    }
  end

  def format_for_query
    @format == :gif ? 1 : 0
  end

  def date_for_query(time)
    (time - time_for_query(time)).to_i
  end

  def time_for_query(time)
    time.hour * HOURS + time.min * MINUTES + time.sec * SECONDS
  end

  def start_delta(time)
    # Used to compensate a time related bug in blitzornung
    time.localtime.utc_offset
  end

  def html_url
    "#{BASE_URL}?#{URI.encode_www_form(query_parameters)}"
  end
end

require 'date'

gif = if ARGV[0]
        # p DateTime.parse(ARGV[0]).to_time.utc
        ThunderGif.new(end_date: DateTime.parse(ARGV[0]).to_time.utc)
      else
        ThunderGif.new
      end

# p gif
# puts gif.html_url
download(gif.html_url, 'tmp---page.html')

gif_url = 'http://fr.blitzortung.org/' +
          `cat tmp---page.html | grep Tmp | sed "s/.* src=\\"\\(PHP.*\\)\\" alt.*/\\1/"`

`rm tmp---page.html`
download(gif_url, "#{gif.start_date.strftime("%Y%m%d%H%M")}.gif")
