# Blirtzortnung.org

Blirtzortnung.org is a website for tracking thunder.

This repository contains tools for automating gif creation from this site.

## thunder-history-to-gif.rb

This is a tool that take a date as parameter (defaulting to now) and generates
one day of lightning activity history. Most parameters are hardcoded.

Currently, this is set to generate lightning activity history for south west
europe, for one day, with a step every 30 minutes. Each image represents the
lightning activity during 2 hours.

### Usage

To generate a gif for the last day, simply run this command without parameter:

```
ruby thunder-history-to-gif.rb
```

If you want to generate history for a specific day, you can do this that way;

```
ruby thunder-history-to-gif.rb "2018-05-06T22:00 +0200"
```

Be careful. The website is not very reliable, and it is possible to get
corrupted data. So, be sure to double check your files once generated and
generate again if necessary.

### Advanced Usage

Because of limitations of the website, we cannot reliably generate more than
one day of history with this resolution (more than 2 hours activity for each
frame, and more than 50 frames par image).

Even doing this, it is still possible to end with corrupted data.

However, we can generate gifs for multiple days, and merge these with gifsicle.
You will need gifsicle installed. You can do it with your preferred package
manager.

First, generate the gif by executing the usual command with consecutive dates:

```
ruby thunder-history-to-gif.rb "2018-05-06T22:00 +0200"
ruby thunder-history-to-gif.rb "2018-05-05T22:00 +0200"
ruby thunder-history-to-gif.rb "2018-05-04T22:00 +0200"
ruby thunder-history-to-gif.rb "2018-05-03T22:00 +0200"
ruby thunder-history-to-gif.rb "2018-05-02T22:00 +0200"
```

Then use gifsicle to merge them. As the website, insert an extra frame at the
end of the gif that is redundant with the next gif, we use gifsicle to exclude
the extra frame as well from the merged file:

```
gifsicle -o 2018_05_01-05.gif \
         201805012000.gif "#0--2" 201805022000.gif "#0--2" 201805032000.gif "#0--2" \
         201805042000.gif "#0--2" 201805052000.gif
```

You got the merged gif.

You can go further and use ffmpeg to get a video from it:

```
ffmpeg -i "2018_05_01-05.gif" -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" "2018_05_01-05.gif.mp4"
```

And here are examples of what you can obtain:

GIF version:

![Lightning activity in May 2018](examples/2018-05-01〜27-2000.gif "Lightning activity in May 2018")

MP4 version:

![Lightning activity in May 2018](examples/example.mp4)

That's it!
